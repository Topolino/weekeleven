# Name: Joey Carberry
# Date: December 4, 2015
# Project: Make an accurate clock
import datetime

t = datetime.time(1, 2, 3)
print(t)
print('hour  :', t.hour)
print('minute:', t.minute)
print('second:', t.second)