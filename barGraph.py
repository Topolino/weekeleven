# Name: Joey Carberry
# Date: December 7, 2015
# Project: Create a simple Bar Graph

from tkinter import *
class barGraph:
    def drawGraph(self):
        self.canvas.delete('bar1', 'bar2', 'bar3', 'bar4', 'text1', 'text2', 'text3', 'text4',
                           'percentText1', 'percentText2', 'percentText3', 'percentTex41', 'titleName1',
                           'bar1Side', 'bar2Side', 'bar3Side', 'bar4Side', 'averageLine')

        totalSum = self.bar1Val.get() + self.bar2Val.get() + self.bar3Val.get() + self.bar4Val.get()
        bar1Percent = float(self.bar1Val.get() / totalSum * 100)
        bar2Percent = float(self.bar2Val.get() / totalSum * 100)
        bar3Percent = float(self.bar3Val.get() / totalSum * 100)
        bar4Percent = float(self.bar4Val.get() / totalSum * 100)

        bar1Height = 500 - (bar1Percent * 5)
        bar2Height = 500 - (bar2Percent * 5)
        bar3Height = 500 - (bar3Percent * 5)
        bar4Height = 500 - (bar4Percent * 5)

        averageHeight = (bar1Height + bar2Height + bar3Height + bar4Height) / 4
        print(averageHeight)

        # self.canvas.create_line(0, averageHeight, 500, averageHeight, fill = 'red', tags = 'averageLine')

        self.canvas.create_rectangle(2, bar1Height, 98, 500, fill = '#1abc9c', tags = 'bar1')
        self.canvas.create_rectangle(95, bar1Height, 98, 500, fill = '#16a085', tags = 'bar1Side')

        self.canvas.create_rectangle(102, bar2Height, 198, 500, fill = '#2ecc71', tags = 'bar2')
        self.canvas.create_rectangle(195, bar2Height, 198, 500, fill = '#27ae60', tags = 'bar2Side')

        self.canvas.create_rectangle(202, bar3Height, 298, 500, fill = '#3498db', tags = 'bar3')
        self.canvas.create_rectangle(295, bar3Height, 298, 500, fill = '#2980b9', tags = 'bar3Side')

        self.canvas.create_rectangle(303, bar4Height, 398, 500, fill = '#9b59b6', tags = 'bar4')
        self.canvas.create_rectangle(395, bar4Height, 398, 500, fill = '#8e44ad', tags = 'bar4Side')

        bar1PerStr = str(round(bar1Percent))
        bar2PerStr = str(round(bar2Percent))
        bar3PerStr = str(round(bar3Percent))
        bar4PerStr = str(round(bar4Percent))

        self.canvas.create_text(50, bar1Height - 30, text = self.bar1Name.get(), tags = 'text1')
        self.canvas.create_text(50, bar1Height - 10, text = bar1PerStr + '%', tags = 'percentText1')

        self.canvas.create_text(150, bar2Height - 30, text = self.bar2Name.get(), tags = 'text2')
        self.canvas.create_text(150, bar2Height - 10, text = bar2PerStr + '%', tags = 'percentText1')

        self.canvas.create_text(250, bar3Height - 30, text = self.bar3Name.get(), tags = 'text3')
        self.canvas.create_text(250, bar3Height - 10, text = bar3PerStr + '%', tags = 'percentText1')

        self.canvas.create_text(350, bar4Height - 30, text = self.bar4Name.get(), tags = 'text4')
        self.canvas.create_text(350, bar4Height - 10, text = bar4PerStr + '%', tags = 'percentText1')

        self.canvas.create_text(200, 30, text = str(self.titleName.get()), tags = 'titleName1')

    def createLineGraph(self):
        totalSum = self.bar1Val.get() + self.bar2Val.get() + self.bar3Val.get() + self.bar4Val.get()
        bar1Percent = float(self.bar1Val.get() / totalSum * 100)
        bar2Percent = float(self.bar2Val.get() / totalSum * 100)
        bar3Percent = float(self.bar3Val.get() / totalSum * 100)
        bar4Percent = float(self.bar4Val.get() / totalSum * 100)

        bar1Height = 500 - (bar1Percent * 5)
        bar2Height = 500 - (bar2Percent * 5)
        bar3Height = 500 - (bar3Percent * 5)
        bar4Height = 500 - (bar4Percent * 5)

        self.canvas.create_oval(49, bar1Height - 1, 51, bar1Height + 1, fill = 'red')

    def __init__(self):
        # Creates the Window and gives it a title
        window = Tk()
        window.title('Bar Graph')

        self.canvas = Canvas(window, height = 500, width = 400, bg = '#ecf0f1')
        self.canvas.pack()

        frame = Frame(window)
        frame.pack()

        Label(frame, text = 'Name:').grid(row = 2, column = 1, sticky = W)
        Label(frame, text = 'Name:').grid(row = 3, column = 1, sticky = W)
        Label(frame, text = 'Name:').grid(row = 4, column = 1, sticky = W)
        Label(frame, text = 'Name:').grid(row = 5, column = 1, sticky = W)

        self.titleName = StringVar()
        Entry(frame, textvariable = self.titleName).grid(row = 1, column = 2, sticky = W)
        Label(frame, text = 'Graph Title').grid(row = 1, column = 1, sticky = W)

        self.bar1Name = StringVar()
        Entry(frame, textvariable = self.bar1Name).grid(row = 2, column = 2, sticky = W)
        self.bar2Name = StringVar()
        Entry(frame, textvariable = self.bar2Name).grid(row = 3, column = 2, sticky = W)
        self.bar3Name = StringVar()
        Entry(frame, textvariable = self.bar3Name).grid(row = 4, column = 2, sticky = W)
        self.bar4Name = StringVar()
        Entry(frame, textvariable = self.bar4Name).grid(row = 5, column = 2, sticky = W)

        self.bar1Val = IntVar()
        Entry(frame, textvariable = self.bar1Val).grid(row = 2, column = 3, sticky = W)
        self.bar2Val = IntVar()
        Entry(frame, textvariable = self.bar2Val).grid(row = 3, column = 3, sticky = W)
        self.bar3Val = IntVar()
        Entry(frame, textvariable = self.bar3Val).grid(row = 4, column = 3, sticky = W)
        self.bar4Val = IntVar()
        Entry(frame, textvariable = self.bar4Val).grid(row = 5, column = 3, sticky = W)

        Button(frame, text = 'Create Graph', command = self.drawGraph()).grid(row = 6, column = 3)
        Button(frame, text = 'Create Line Graph', command = self.createLineGraph()).grid(row = 6, column = 2)

        window.mainloop()

barGraph()