# Name: Joey Carberry
# Date: December 1, 2015
# Project: Ball Creation

from tkinter import *
class CanvasDemo:

    def moveUp(self):
        self.canvas.move('oval', 0, -5)
    def moveDown(self):
        self.canvas.move('oval', 0, 5)
    def moveLeft(self):
        self.canvas.move('oval', -5, 0)
    def moveRight(self):
        self.canvas.move('oval', 5, 0)

    def __init__(self):
        window = Tk()
        window.title('Canvas Demo')

        # Place Canvas on the demo
        self.canvas = Canvas(window, width = 350, height = 350, bg = 'white')
        self.canvas.pack()

        # Place Frame into demo
        frame = Frame(window)
        frame.pack()

        Button(frame, text = 'Move Up', command = self.moveUp).grid(row = 1, column = 1)
        Button(frame, text = 'Move Down', command = self.moveDown).grid(row = 1, column = 2)
        Button(frame, text = 'Move Left', command = self.moveLeft).grid(row = 1, column = 3)
        Button(frame, text = 'Move Right', command = self.moveRight).grid(row = 1, column = 4)
        Button(frame, text = 'Start', command = self.displayCircle).grid(row = 2, column = 1)

        window.mainloop()

    def displayCircle(self):
        self.canvas.create_oval(100, 100, 400, 400, fill = 'blue', tags = 'oval')
        print('Run')
CanvasDemo()
