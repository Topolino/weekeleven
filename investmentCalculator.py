# Name: Joey Carberry
# Date: December 2, 2015
# Project: Investment Calculator

from tkinter import *
class InvestmentCalculator:
    def computeFutureValue(self):
        interestRate = float(self.interestRate.get()) / 1200
        f = float(self.investmentAmount.get()) * \
            (1 + interestRate) ** (float(self.years.get()) * 12)
        self.futureValue.set("{0:10.2f}".format(f))

    def __init__(self):
        window = Tk()
        window.title('Investment Calculator')

        frame = Frame(window)
        frame.pack()

        # Creates Labels
        Label(frame, text = 'Investment Amount').grid(row = 1, column = 1, sticky = E)
        Label(frame, text = 'Years').grid(row = 2, column = 1, sticky = E)
        Label(frame, text = 'Annual Interest Rate').grid(row = 3, column = 1, sticky = E)
        self.futureValue = StringVar()
        Label(frame, text = 'Future Value', textvariable = self.futureValue, justify = RIGHT).grid(row = 5, column = 2, sticky = W)

        # Creates Button and Entries
        self.investmentAmount = StringVar()
        Entry(frame, textvariable = self.investmentAmount).grid(row = 1, column = 2, sticky = W)
        self.years = StringVar()
        Entry(frame, textvariable = self.years).grid(row = 2, column = 2, sticky = W)
        self.interestRate = StringVar()
        Entry(frame, textvariable = self.interestRate).grid(row = 3, column = 2, sticky = W)
        Button(frame, text = 'Calculate', command = self.computeFutureValue).grid(row = 4, column = 2)

        window.mainloop()

InvestmentCalculator()