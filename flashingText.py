# Name: Joey Carberry
# Date: December 4, 2015
# project: Create a flashing text box

from tkinter import *
import time
class flashText:
    def __init__(self):
        # Creates the self.Window and gives it a title
        self.window = Tk()
        self.window.title('Flashing Text')

        canvas = Canvas(self.window, width = 300, height = 300)
        canvas.pack()

        canvas.create_text(100, 100, text = 'Welcome', tags = 'text')
        time.sleep(2)
        print('After sleep')
        canvas.delete('text')

        self.window.mainloop()


flashText()