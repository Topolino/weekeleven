# Name: Joey Carberry
# Date: December 7, 2015
# Project: Create two alternating messages

from tkinter import *
class alternateMessages:
    def __init__(self):
        # Creates the Window and gives it a title
        window = Tk()
        window.title('Alternating Messages')

        self.pH = 1

        self.canvas = Canvas(window, width = 300, height = 300)
        self.canvas.pack()

        self.canvas.bind('<Button-1>', self.changeText)

        window.mainloop()
    def changeText(self, event):
        if self.pH == 1:
            self.canvas.delete('textThing')
            self.canvas.create_text(150, 100, text = 'Programming is fun!', tags = 'textThing')
            self.pH = 0
        else:
            self.canvas.delete('textThing')
            self.canvas.create_text(150, 100, text = 'Programming is hard!', tags = 'textThing')
            self.pH = 1
alternateMessages()