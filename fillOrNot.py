# Name: Joey Carberry
# Date: December 2, 2015
# Project: Fill or Not Fill a Rectangle or Oval

from tkinter import *
class fillOrNot:
    # Clears the Canvas from all shapes
    def clearCanvas(self):
        self.canvas.delete('rect', 'oval')

    # Draws a Rectangle. If the checkbox is checked then it will be filled blue.
    def drawRect(self):
        self.clearCanvas()
        if(self.var1.get()):
            self.canvas.create_rectangle(10,10,190,90, fill = 'blue', tags = 'rect')
        else:
            self.canvas.create_rectangle(10,10,190,90, tags = 'rect')

    # Does the same as drawRect but with a Circle/Oval
    def drawOval(self):
        self.clearCanvas()
        if(self.var1.get()):
            self.canvas.create_oval(75, 75, 200, 200, fill = 'blue', tags = 'oval')
        else:
            self.canvas.create_oval(100, 100, 250, 250, tags = 'oval')

    def __init__(self):
        # Creates the Window and gives it a title
        window = Tk()
        window.title('Fill or Not Fill')

        # Creates the frame where all the buttons are held.
        frame = Frame(window)
        frame.pack()

        # Creates the Canvas where the rectangles and circles are drawn
        self.canvas = Canvas(window, width = 350, height = 350, bg = 'white')
        self.canvas.pack()

        # Makes it so only one Radiobutton can be checked at one time
        b1 = IntVar()
        # Creates the radio buttons
        Radiobutton(frame, text = 'Rectangle', command = self.drawRect, variable = b1, value = 1).grid(row = 1, column = 1)
        Radiobutton(frame, text = 'Oval', command = self.drawOval, variable = b1, value = 2).grid(row = 1, column = 2)

        # Creates check Button
        self.var1 = IntVar()
        Checkbutton(frame, text = 'Filled', variable = self.var1).grid(row = 1, column = 3)

        # Runs the main loop of the buttons
        window.mainloop()

fillOrNot()