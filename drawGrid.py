# Name: Joey Carberry
# Date: December 3, 2015
# Project: Draw a grid with the vertical lines being red and the horizontal being blue

from tkinter import *

class drawGrid:
    def drawTheGrid(self):
        # Vertical Lines
        x1 = 50
        y1 = 20
        # Creates lines that are 10px away from the other horizontally
        for x in range(0, 8):
            self.canvas.create_line(x1, 10, x1, 100, tags = 'line')
            x1 += 10

        # Horizontal Lines
        # Creates lines that are 10px away from the other vertically
        for y in range(0, 8):
            self.canvas.create_line(40, y1, 130, y1, tags = 'line')
            y1 += 10

    def __init__(self):
        # Creates the Window and gives it a title
        window = Tk()
        window.title('Draw Grid')

        # Creates the Canvas for the grid
        self.canvas = Canvas(window, width = 200, height = 100, bg = 'white')
        self.canvas.pack()

        # Creates the Frame for the Button
        frame = Frame(window)
        frame.pack()

        # Creates the Button that launches the grid creation
        Button(frame, text = 'Go', command = self.drawTheGrid).grid(row = 1, column = 1)

        # Runs the Main loop
        window.mainloop()

drawGrid()