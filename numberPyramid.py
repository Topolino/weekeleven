# Name: Joey Carberry
# Date: December 3, 2015
# Project: Number Pyramid - Displays sequential numbers in a pyramid formation that resizes to the window

from tkinter import *
class numberPyramid:
    def __init__(self):
        # Creates the Window and gives it a title
        window = Tk()
        window.title('Number Pyramid')

        # Creates the Canvas where the number pyramid is placed
        canvasHeight = 350
        self.canvas = Canvas(window, width = 350, height = canvasHeight, bg = 'white')
        self.canvas.pack()
        labelText = ''
        y = 1
        max = canvasHeight / 25 + 1
        max = int(max)
        for x in range(1, max): # 22
            labelText += str(x) + ' '
            Label(self.canvas, text = labelText).grid(row = y, column = 1)
            y += 1

        window.mainloop()

numberPyramid()