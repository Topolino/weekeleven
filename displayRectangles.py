# Name: Joey Carberry
# Date: December 3, 2015
# Project: Display Rectangle: Displays 20 rectangles inside each other

from tkinter import *

class drawRepeatRect:
    # Draws the repeated rectangles, called by a button
    def drawRect(self):
        # Sets the x and y values for the rectangle
        x1 = 1
        y1 = 1
        x2 = 400
        y2 = 400
        # Draws rectangles with a 1 pixel buffer between them inward
        # Draws the rectangles 100 times instead of 20, because why not?
        for x in range(0,101):
            self.canvas.create_rectangle(x1,y1,x2,y2, tags = 'rect')
            x1 += 2
            y1 += 2
            x2 -= 2
            y2 -= 2

    def __init__(self):
        # Creates the Window and gives it a title
        window = Tk()
        window.title('Draw Rectangle')

        # Creates the frame for the Button
        frame = Frame(window)
        frame.pack()

        # Creates the canvas for the rectangles, it is 401x401px so that the original x2 and y2 values will be nice
        self.canvas = Canvas(window, width = 401, height = 401, bg = 'white')
        self.canvas.pack()

        # Creates a launch button to add effect
        Button(frame, text = 'Go', command = self.drawRect).grid(row = 1, column = 1)

        # Runs the main loop
        window.mainloop()

drawRepeatRect()