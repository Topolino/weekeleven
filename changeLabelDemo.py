# Name: Joey Carberry
# Date: December 1, 2015
# Project: Changing Label and Color

from tkinter import *

class ChangeLabelDemo:
    def __init__(self):
        window = Tk() # Creates a window
        window.title('Change Label Demo') # Sets the title of the Demo
        # Add a label to the frame
        frame1 = Frame(window) # Create and add frame to window
        frame1.pack()
        self.lbl = Label(frame1, text = 'Programming is fun')
        self.lbl.pack()

        # Add a label, entry, button, and two radio buttons to frame2
        frame2 = Frame(window) # Create second frame and add to window
        frame2.pack()
        label = Label(frame2, text = 'Enter Text')
        self.msg = StringVar()
        entry = Entry(frame2, textvariable = self.msg)
        btChangeText = Button(frame2, text = 'ChangeText', command = self.processButton)
        self.v1 = StringVar()
        rbBlue = Radiobutton(frame2, text = 'Blue', bg = 'blue', variable = self.v1, value = 'B',
                            command = self.processRadiobutton)
        rbOrange = Radiobutton(frame2, text = 'Orange', bg = 'orange', variable = self.v1, value = 'O',
                               command = self.processRadiobutton)
        rbGreen = Radiobutton(frame2, text = 'Green', bg = 'green', variable = self.v1, value = 'G',
                               command = self.processRadiobutton)
        rbRed = Radiobutton(frame2, text = 'Red', bg = 'red', variable = self.v1, value = 'R',
                               command = self.processRadiobutton)

        label.grid(row = 1, column = 1)
        entry.grid(row = 1, column = 2)
        btChangeText.grid(row = 1, column = 3)
        rbBlue.grid(row = 1, column = 4)
        rbOrange.grid(row = 1, column = 5)
        rbGreen.grid(row = 1, column = 6)
        rbRed.grid(row = 1, column = 7)
        window.mainloop()


    def processRadiobutton(self):
        if self.v1.get() == 'B':
            self.lbl['fg'] = 'blue'
        elif self.v1.get() == 'O':
            self.lbl['fg'] = 'Orange'
        elif self.v1.get() == 'G':
            self.lbl['fg'] = 'green'
        elif self.v1.get() == 'R':
            self.lbl['fg'] = 'red'
    def processButton(self):
        self.lbl['text'] = self.msg.get() # New Text for the label

ChangeLabelDemo()


